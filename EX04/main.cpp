#include "PlainText.h"
#include "ShiftText.h"
#include "CaesarText.h"
#include "SubstitutionText.h"
#include "FileHelper.h"

#include <iostream>
#include <string>
#include <fstream>

using std::cout;
using std::endl;
using std::cin;


int main()
{
	string str = "";
	string str2 = "";
	int choice = 0;
	int key = 0;
	string dictionaryFileName = "";
	int choice1 = 0;
	int choice2 = 0;
	unsigned int i = 0;
	//declaring vars
	while (true)
	{
		while (choice < 1 || choice > 4)
		{
			cout << "1. Decrypt a string" << endl;
			cout << "2. Decrypt/Encrypt a text file" << endl;
			cout << "3. Show the amount of texts that were recieved in the program" << endl;
			cout << "4. Exit" << endl;
			cout << "Enter your choice: ";
			cin >> choice;
			if (choice < 1 || choice > 4)
			{
				cout << "Invalid choice!" << endl;
				cout << "\n";
			}
		}
		cout << "\n\n";
		//getting choice from user
		switch (choice)
		{
			case 1:
				cout << "Enter a string: ";
				std::getline(std::cin >> std::ws, str);
				//getting string to encrypt from user
				choice = 0;
				//resetting choice
				while (choice < 1 || choice > 3)
				{
					cout << "1. Shift" << endl;
					cout << "2. Caesar" << endl;
					cout << "3. Substitution" << endl;
					cout << "Enter your choice: ";
					cin >> choice;
					if (choice < 1 || choice > 3)
					{
						cout << "Invalid choice!" << endl;
						cout << "\n";
					}
				}
				//getting choice from user
				if(choice == 1)
				{
					cout << "Enter shift key: ";
					cin >> key;
					//getting shift key from user
					ShiftText shiftedStr = ShiftText(str, key);
					shiftedStr.decrypt();
					shiftedStr.decrypt();
					cout << "Decrypted string: " << shiftedStr.getText() << endl;
				}
				else if(choice == 2)
				{

					CaesarText caesarStr = CaesarText(str);
					caesarStr.decrypt();
					caesarStr.decrypt();
					cout << "Decrypted string: " << caesarStr.getText() << endl;
				}
				else if (choice == 3)
				{
					cout << "Enter dictionary file name: ";
					cin >> dictionaryFileName;
					//getting dictionary file name from user
					SubstitutionText substitutionStr = SubstitutionText(str, dictionaryFileName);
					substitutionStr.decrypt();
					cout << "Decrypted string: " << substitutionStr.decrypt() << endl;
				}
				cout << "\n\n";
				break;
			case 2:
				cout << "Enter text-file name: ";
				cin >> str;
				//getting text-file name from user
				choice = 0;
				//resetting choice
				while (choice < 1 || choice > 2)
				{
					cout << "1. Encrypt" << endl;
					cout << "2. Decrypt" << endl;
					cout << "Enter your choice: ";
					cin >> choice;
					if (choice < 1 || choice > 2)
					{
						cout << "Invalid choice!" << endl;
						cout << "\n";
					}
				}
				//getting choice from user
				while (choice1 < 1 || choice1 > 3)
				{
					cout << "1. Shift" << endl;
					cout << "2. Caesar" << endl;
					cout << "3. Substitution" << endl;
					cout << "Enter your choice: ";
					cin >> choice1;
					if (choice1 < 1 || choice1 > 3)
					{
						cout << "Invalid choice!" << endl;
						cout << "\n";
					}
				}
				//getting choice from user
				while (choice2 < 1 || choice2 > 2)
				{
					cout << "1. Save to new text file" << endl;
					cout << "2. Print text" << endl;
					cout << "Enter your choice: ";
					cin >> choice2;
					if (choice2 < 1 || choice2 > 2)
					{
						cout << "Invalid choice!" << endl;
						cout << "\n";
					}
				}
				//getting choice from user
				FileHelper help;
				str2 = help.readFileToString(str);
				//getting file content
				if(choice1 == 1)
				{
					cout << "Enter shift key: ";
					cin >> key;
					//getting shift key from user
					ShiftText shiftedStr = ShiftText(str2, key);
					if (choice == 1)
					{
						if (choice2 == 1)
						{
							cout << "Enter name of the text-file you want to save the encrypted string to: ";
							cin >> str;
							//getting text-file path
							std::ofstream outputFile(str);
							str = shiftedStr.getText();
							for (i = 0; i < str.length(); i++)
							{
								if (str[i] == ' ')
								{
									outputFile << "\n";
								}
								else
								{
									outputFile << str[i];
								}
							}
						}
						else
						{
							cout << "Encrypted string: " << shiftedStr.getText() << endl;
						}
					}
					else
					{
						if (choice2 == 1)
						{
							cout << "Enter name of the text-file you want to save the decrypted string to: ";
							cin >> str;
							//getting text-file path
							std::ofstream outputFile(str);
							shiftedStr.decrypt();
							shiftedStr.decrypt();
							str = shiftedStr.getText();
							for (i = 0; i < str.length(); i++)
							{
								if (str[i] == ' ')
								{
									outputFile << "\n";
								}
								else
								{
									outputFile << str[i];
								}
							}
						}
						else
						{
							shiftedStr.decrypt();
							shiftedStr.decrypt();
							cout << "Decrypted string: " << shiftedStr.getText() << endl;

						}
					}
				}
				else if (choice1 == 2)
				{
					CaesarText caesarStr = CaesarText(str2);
					if (choice == 1)
					{
						if (choice2 == 1)
						{
							cout << "Enter name of the text-file you want to save the encrypted string to: ";
							cin >> str;
							//getting text-file path
							std::ofstream outputFile(str);
							str = caesarStr.getText();
							for (i = 0; i < str.length(); i++)
							{
								if (str[i] == ' ')
								{
									outputFile << "\n";
								}
								else
								{
									outputFile << str[i];
								}
							}
						}
						else
						{
							cout << "Encrypted string: " << caesarStr.getText() << endl;
						}
					}
					else
					{
						if (choice2 == 1)
						{
							cout << "Enter name of the text-file you want to save the decrypted string to: ";
							cin >> str;
							//getting text-file path
							std::ofstream outputFile(str);
							caesarStr.decrypt();
							caesarStr.decrypt();
							str = caesarStr.getText();
							for (i = 0; i < str.length(); i++)
							{
								if (str[i] == ' ')
								{
									outputFile << "\n";
								}
								else
								{
									outputFile << str[i];
								}
							}
						}
						else
						{
							caesarStr.decrypt();
							caesarStr.decrypt();
							cout << "Decrypted string: " << caesarStr.getText() << endl;

						}
					}
				}
				else
				{
					cout << "Enter dictionary file name: ";
					cin >> dictionaryFileName;
					//getting dictionary file name from user
					SubstitutionText substitutionStr = SubstitutionText(str2, dictionaryFileName);
					if (choice == 1)
					{
						if (choice2 == 1)
						{
							cout << "Enter name of the text-file you want to save the encrypted string to: ";
							cin >> str;
							//getting text-file path
							std::ofstream outputFile(str);
							str = substitutionStr.getText();
							for (i = 0; i < str.length(); i++)
							{
								if (str[i] == ' ')
								{
									outputFile << "\n";
								}
								else
								{
									outputFile << str[i];
								}
							}
						}
						else
						{
							cout << "Encrypted string: " << substitutionStr.getText() << endl;
						}
					}
					else
					{
						if (choice2 == 1)
						{
							cout << "Enter name of the text-file you want to save the decrypted string to: ";
							cin >> str;
							//getting text-file path
							std::ofstream outputFile(str);
							substitutionStr.decrypt();
							substitutionStr.decrypt();
							str = substitutionStr.getText();
							for (i = 0; i < str.length(); i++)
							{
								if (str[i] == ' ')
								{
									outputFile << "\n";
								}
								else
								{
									outputFile << str[i];
								}
							}
						}
						else
						{
							substitutionStr.decrypt();
							cout << "Decrypted string: " << substitutionStr.decrypt() << endl;
						}
					}
				}
				choice1 = 0;
				choice2 = 0;
				//resetting choices
				break;
			case 3: 
				cout << "Amount of texts that were recieved in the program: ";
				cout << PlainText::getNumOfTexts() << endl;
				cout << "\n" << endl;
				break;
			default:
				_exit(1);
		}
		choice = 0;
		//resetting choice
	}
	return 0;
}