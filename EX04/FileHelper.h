#pragma once

#include <iostream>


using std::string;


class FileHelper
{
	public:
		static string readFileToString(string fileName);
		static void writeWordsToFile(string inputFileName, string outputFileName);
};

