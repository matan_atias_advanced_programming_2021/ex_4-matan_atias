#pragma once

#include <iostream>

using std::string;


class PlainText
{
	private:
		static int _NumOfTexts;
	protected:
		//fields:
		string text;
		bool isEncrypted;
	public:
		//methods:
		PlainText(string text); //constructor
		~PlainText(); //destructor
		bool isEnc() const;
		string getText() const;
		static int getNumOfTexts();
		friend std::ostream& operator<<(std::ostream& os, PlainText& text);
};