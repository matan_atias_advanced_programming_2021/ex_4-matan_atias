#include "PlainText.h"
#include <iostream>
#include <string>

using std::string;

int PlainText::_NumOfTexts = 0;

/*
	Constructor
	Input:	
		text
	Output:
		None
*/
PlainText::PlainText(string text)
{
	this->_NumOfTexts++;
	this->text = text;
	this->isEncrypted = false;
}


/*
	Destructor
	Input:
		None
	Output:
		None
*/
PlainText::~PlainText()
{

}


/*
	Function checks if text is encrypted
	Input:
		None
	Output:
		True if encrypted, False if not encrypted
*/
bool PlainText::isEnc() const
{
	return this->isEncrypted;
}


/*
	Function returns the text
	Input:
		None
	Output:
		text
*/
string PlainText::getText() const
{
	return this->text;
}


/*
	Function returns num of texts created
	Input:
		None
	Output:
		Number of texts created
*/
int PlainText::getNumOfTexts()
{
	return _NumOfTexts;
}


std::ostream& operator<<(std::ostream& os, PlainText& text)
{
	os << "PlainText\n";
	os << text.getText() << std::endl;
	return os;
}
