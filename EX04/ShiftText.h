#pragma once

#include "PlainText.h"
#include <iostream>

using std::string;


class ShiftText : public PlainText
{
	public:
		//methods:
		ShiftText(string text, int key);//constructor
		~ShiftText(); //destructor
		string encrypt();
		string decrypt();
		friend std::ostream& operator<<(std::ostream& os, ShiftText& text);
	private:
		//fields:
		int _key;
};