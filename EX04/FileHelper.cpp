#include "FileHelper.h"
#include <iostream>
#include <fstream>
#include <string>

using std::string;


/*
	Function reads from a file and writes to a string
	Input:
		input file name
	Output:
		string with the file's content
*/
string FileHelper::readFileToString(string fileName)
{
	std::ifstream file(fileName); //file pointer
	string word = "";
	string fileContent = "";
	//declaring vars
	while (file >> word)
	{
		fileContent += word;
		fileContent += " ";
	}
	return fileContent;
}


/*
	Functions reads from a file and writes to another file
	Input:
		input/output file name
	Output:
		None
*/
void FileHelper::writeWordsToFile(string inputFileName, string outputFileName)
{
	std::ofstream outputFile(outputFileName);
	string input = readFileToString(inputFileName);
	unsigned int i = 0;
	//declaring vars
	for (i = 0; i < input.length(); i++)
	{
		if (input[i] == ' ')
		{
			outputFile << "\n";
		}
		else
		{
			outputFile << input[i];
		}
	}
}	
