#include "ShiftText.h"
#include <iostream>
#include <string>

using std::string;


/*
	Constructor
	Input:
		text, encryption key
	Output:
		None
*/
ShiftText::ShiftText(string text, int key) :
	PlainText(text), _key(key)
{
	this->text = encrypt();
	if (this->_key > 26)
	{
		this->_key = this->_key % 26;
	}
}


/*
	Destructor
	Input:
		None
	Output:
		None
*/
ShiftText::~ShiftText()
{

}


/*
	Function encrypts the text using the encryption key
	Input:
		None
	Output:
		encrypted text
*/
string ShiftText::encrypt()
{
	string encrypted_text = "";
	unsigned int i = 0;
	//declaring vars
	for (i = 0; i < text.length(); i++)
	{
		if (isalpha(this->text[i]))
		{
			encrypted_text += int(this->text[i] + this->_key - 97) % 26 + 97;
		}
		else
		{
			encrypted_text += this->text[i];
		}
	}
	this->text = encrypted_text;
	this->isEncrypted = true;
	return encrypted_text;
}


/*
	Function decrypts the text using the encryption key
	Input:
		None
	Output:
		Decrypted text
*/
string ShiftText::decrypt()
{
	string decrypted_text = "";
	unsigned int i = 0;
	//declaring vars
	for (i = 0; i < this->text.length(); i++)
	{
		if (isalpha(this->text[i]))
		{
			if (this->text[i] < 97 + this->_key)
			{
				decrypted_text += 122 + 1 + this->text[i] - this->_key - 97;
			}
			else
			{
				decrypted_text += int(this->text[i] - this->_key - 97) % 26 + 97;
			}
		}
		else
		{
			decrypted_text += this->text[i];
		}
	}
	this->text = decrypted_text;
	this->isEncrypted = false;
	return decrypted_text;
}


std::ostream& operator<<(std::ostream& os, ShiftText& text)
{
	ShiftText s = text;
	//declaring vars
	os << "ShiftText\n";
	if (s.isEnc())
	{
		os << text.getText() << std::endl;
	}
	else
	{
		s.encrypt();
		os << text.getText() << std::endl;
	}
	return os;
}