#pragma once

#include "PlainText.h"
#include <iostream>

using std::string;


class SubstitutionText : public PlainText
{
	public:
		//methods:
		SubstitutionText(string text, string dictionaryFileName); //constructor
		~SubstitutionText(); //destructor
		string encrypt();
		string decrypt();
		friend std::ostream& operator<<(std::ostream& os, SubstitutionText& text);
	private:
		//fields:
		string _dictionaryFileName;
};