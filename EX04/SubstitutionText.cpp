#include "SubstitutionText.h"
#include <iostream>
#include <fstream>
#include <string>

using std::string;


/*
	Constructor
	Input:
		text, dictionary file name
	Output:
		None
*/
SubstitutionText::SubstitutionText(string text, string dictionaryFileName) :
	PlainText(text)
{
	this->_dictionaryFileName = dictionaryFileName;
	encrypt();
}


/*
	Destructor
	Input:
		None
	Output:
		None
*/
SubstitutionText::~SubstitutionText()
{

}


/*
	Function encrypts the text using the encryption key
	Input:
		None
	Output:
		encrypted text
*/
string SubstitutionText::encrypt()
{
	std::ifstream file(this->_dictionaryFileName); //file pointer
	string line = "";
	unsigned int i = 0;
	string encryptedText = "";
	bool flag = true;
	//declaring vars
	if (file)
	{
		for (i = 0; i < this->text.length(); i++)
		{
			if (isalpha(this->text[i]))
			{
				while (std::getline(file, line) && flag)
				{
					if (line[0] == this->text[i])
					{
						flag = false;
						encryptedText += line[2];
					}
				}
				file.seekg(0);
			}
			else
			{
				encryptedText += this->text[i];
			}
			flag = true;
		}
	}
	file.close();
	this->text = encryptedText;
	this->isEncrypted = true;
	return encryptedText;
}


/*
	Function decrypts the text using the encryption key
	Input:
		None
	Output:
		Decrypted text
*/
string SubstitutionText::decrypt()
{
	std::ifstream file(this->_dictionaryFileName); //file pointer
	string line = "";
	unsigned int i = 0;
	string decryptedText = "";
	bool flag = true;
	//declaring vars
	if (file)
	{
		for (i = 0; i < this->text.length(); i++)
		{
			if (isalpha(this->text[i]))
			{
				while (std::getline(file, line) && flag)
				{
					if (line[2] == this->text[i])
					{
						flag = false;
						decryptedText += line[0];
					}
				}
				file.seekg(0);
			}
			else
			{
				decryptedText += this->text[i];
			}
			flag = true;
		}
	}
	file.close();
	this->text = decryptedText;
	this->isEncrypted = false;
	return decryptedText;
}


std::ostream& operator<<(std::ostream& os, SubstitutionText& text)
{
	SubstitutionText s = text;
	//declaring vars
	os << "Substitution\n";
	if (s.isEnc())
	{
		os << text.getText() << std::endl;
	}
	else
	{
		s.encrypt();
		os << text.getText() << std::endl;
	}
	return os;
}