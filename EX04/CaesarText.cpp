#include "CaesarText.h"
#include <iostream>
#include <string>

using std::string;


/*
	Constructor
	Input:
		text
	Output:
		None
*/
CaesarText::CaesarText(string text) :
	ShiftText(text, 3)
{

}


/*
	Destructor
	Input:
		None
	Output:
		None
*/
CaesarText::~CaesarText()
{

}


/*
	Function encrypts the text using the encryption key
	Input:
		None
	Output:
		encrypted text
*/
string CaesarText::encrypt()
{
	this->text = ShiftText::encrypt();
	this->isEncrypted = true;
	return text;
}


std::ostream& operator<<(std::ostream& os, CaesarText& text)
{
	CaesarText s = text;
	//declaring vars
	os << "CaesarText\n";
	if (s.isEnc())
	{
		os << text.getText() << std::endl;
	}
	else
	{
		s.encrypt();
		os << text.getText() << std::endl;
	}
	return os;
}