#pragma once

#include "ShiftText.h"
#include <iostream>

using std::string;


class CaesarText : public ShiftText
{
	public:
		//methods
		CaesarText(string text); //constructor
		~CaesarText(); //destructor
		string encrypt();
		friend std::ostream& operator<<(std::ostream& os, CaesarText& text);
		/*
		Function decrypts the text using the encryption key
		Input:
			None
		Output:
			Decrypted text
		*/
		string decrypt()
		{
			ShiftText::decrypt();
			this->isEncrypted = false;
			return this->text;
		}
};